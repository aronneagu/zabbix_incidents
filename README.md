This script will check Zabbix and get the number of incidents in the ZABBIX_INCIDENTS variable. The variable can be added to bash prompt PS1

It depends on jq package, a JSON parser, which can be installed with
sudo apt-get install jq 
