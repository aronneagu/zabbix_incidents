#!/bin/bash
zabbix_server=""
zabbix_auth_token=""
function get_zabbix_incidents (){
echo $(curl -X POST --silent --header "Content-type:application/json-rpc" --data '{"jsonrpc": "2.0","method": "trigger.get","params":{"output":"extend","filter":{"value":1},"countOutput":"true"},"id": 1,"auth": "'"${zabbix_auth_token}"'"}' ${zabbix_server}:80/zabbix/api_jsonrpc.php 2>/dev/null|jq '.result' --raw-output)
}
# if number of incidents is 0 then ZABBIX_INCIDENTS variable has empty value, otherwise it contains the number of active incidents
ZABBIX_INCIDENTS=$(get_zabbix_incidents)
if [ $ZABBIX_INCIDENTS = "0" ]
    then 
    ZABBIX_INCIDENTS=""
fi
